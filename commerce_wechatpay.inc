<?php

/**
 * @file
 * Helper classes.
 */

use EasyWeChat\Factory;

/**
 * Class CommerceWechatpay.
 *
 * Main handler class for Commerce wechatpay, you can extend this class
 * and set your own name using hook_commerce_wechatpay_class_name_alter()
 *
 * @see hook_commerce_wechatpay_class_name_alter()
 * @see commerce_wechatpay_get_handler_class()
 */
class CommerceWechatpay {

  protected $config;

  protected $jsParm;

  /**
   * Constructor.
   */
  public function __construct($options) {

    $this->config = $options;

    return $this;
  }

  /**
   * Prepare all parameters and send to Wechat.
   */
  public function proceed($order_num, $oder_name, $price, $notify_url) {

    $app = Factory::payment($this->config);

    $helper = new CommerceWeChatPayHelper($this->config);

    $openid = $helper->openid;

    // Create order.
    $attributes = [
      // JSAPI，NATIVE，APP...
      'trade_type' => 'JSAPI',
      'body' => $oder_name,
      'out_trade_no' => $this->orderId2outTradeNo($order_num),
      // Unit: Cent.
      'total_fee' => $price,
      'spbill_create_ip' => $this->get_client_ip(),
      'notify_url' => $notify_url,
      'openid' => $openid,
    ];

    $result = $app->order->unify($attributes);
    if ($result['return_code'] == 'SUCCESS' && $result['result_code'] == 'SUCCESS') {
      $prepayId = $result['prepay_id'];
      $this->jsParm = $app->jssdk->bridgeConfig($prepayId);
    }
  }

  /**
   * Get client ip.
   *
   * @return array|mixed
   *   IP address.
   */
  private function get_client_ip() {
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
      return $_SERVER['HTTP_CLIENT_IP'];
    }
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
      return $_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    else {
      return ip_address();
    }
  }

  /**
   * Proceed QR Code pay.
   *
   * @param $order_num
   * @param $oder_name
   * @param $price
   * @param $notify_url
   *
   * @return bool|mixed
   *   QR Code url.
   */
  public function proceed_qrpay($order_num, $oder_name, $price, $notify_url) {

    $app = Factory::payment($this->config);

    // Create order.
    $attributes = [
      // JSAPI，NATIVE，APP...
      'trade_type' => 'NATIVE',
      'body' => $oder_name,
      'out_trade_no' => $this->orderId2outTradeNo($order_num),
      // Unit: Cent.
      'total_fee' => $price,
      'spbill_create_ip' => $this->get_client_ip(),
      'notify_url' => $notify_url,
    ];

    $result = $app->order->unify($attributes);

    watchdog('commerce_wechatpay',
      "Order result: <pre> %result </pre>'",
      ['%result' => var_export($result, TRUE)]
    );

    if ($result['return_code'] == 'SUCCESS' && $result['result_code'] == 'SUCCESS') {
      return $result["code_url"];
    }
    return FALSE;
  }

  /**
   * Generate out trade number from order_id, feel free to extend.
   */
  public function orderId2outTradeNo($order_num) {
    $out_trade_no = $order_num;
    return $out_trade_no;
  }

  /**
   * Get order_id from out trade number, feel free to extend.
   */
  public function outTradeNo2orderId($out_trade_no) {
    $order_num = $out_trade_no;
    return $order_num;
  }

  /**
   * Get JS parameters.
   */
  public function getJsParm($order_num, $oder_name, $price, $notify_url) {
    $this->proceed($order_num, $oder_name, $price, $notify_url);
    return $this->jsParm;
  }

}

/**
 * Helper class for Commerce WeChatPay.
 */
class CommerceWeChatPayHelper {
  private $appid;
  private $appsecret;
  public $openid;

  /**
   * CommerceWeChatPayHelper constructor.
   *
   * @param array $options
   *   Including keys app_id and secret.
   */
  public function __construct(array $options) {
    $this->appid = $options['app_id'];
    $this->appsecret = $options['secret'];
    $this->setOpenid();
  }

  /**
   * Set openid.
   */
  private function setOpenid() {
    // Get openid by code.
    if (!isset($_GET['code'])) {
      $url = $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'] . $_SERVER['QUERY_STRING'];
      if (!empty($_SERVER['HTTPS'])) {
        $url = "https://" . $url;
      }
      else {
        $url = "http://" . $url;
      }
      $code_url = $this->codeUrl($url);
      header("Location: $code_url");
      exit();
    }
    else {
      $code = $_GET['code'];
      $openid = $this->getOpenid($code);
      $this->openid = $openid;
    }
  }

  /**
   * Get openid by code.
   *
   * @return Mixed
   *   Openid or false.
   */
  private function getOpenid($code) {
    $url = $this->openidUrl($code);

    $response = drupal_http_request($url);

    if ($response->code == 200) {
      $data = json_decode($response->data);
      return $data->openid;
    }
    return FALSE;
  }

  /**
   * Get url for requesting code.
   *
   * @return string
   *   Url.
   */
  private function codeUrl($redirectUrl) {
    $parameters = [];
    $parameters["appid"] = $this->appid;
    $parameters["redirect_uri"] = "$redirectUrl";
    $parameters["response_type"] = "code";
    $parameters["scope"] = "snsapi_base";
    $url = url("https://open.weixin.qq.com/connect/oauth2/authorize", array('query' => $parameters));
    return $url . "#wechat_redirect";
  }

  /**
   * Get url for requesting openid by code.
   *
   * @return string
   *   Url.
   */
  private function openidUrl($code) {
    $parameters = [];
    $parameters["appid"] = $this->appid;
    $parameters["secret"] = $this->appsecret;
    $parameters["code"] = $code;
    $parameters["grant_type"] = "authorization_code";
    $url = url("https://api.weixin.qq.com/sns/oauth2/access_token", array('query' => $parameters));
    return $url;
  }

}
